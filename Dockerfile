FROM python:3

COPY . /app/
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINt [ "python", "app.py" ]
#ENTRYPOINT [ "python3", "counter.py" ]
